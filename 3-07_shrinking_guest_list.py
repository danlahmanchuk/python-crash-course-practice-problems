# 3-7. Shrinking Guest List:  "You just found found out that your new dinner table wont't arrive in time for dinner, and you have space for only two guests."

guest_list = ['Albert Einstein', 'Isaac Newton', 'Stephen Hawking']

print(f"{guest_list.pop(2)} won't make it to dinner.")
